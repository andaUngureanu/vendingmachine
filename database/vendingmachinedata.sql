CREATE SCHEMA `vendingmachinedatabase` ;

CREATE TABLE `vendingmachinedatabase`.`coin` (
  `id` INT NOT NULL,
  `type` VARCHAR(45) NULL,
  `description` VARCHAR(45) NULL,
  `value` DOUBLE NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC))
ENGINE = InnoDB
PACK_KEYS = 1;

INSERT INTO `vendingmachinedatabase`.`coin` (`id`, `type`, `description`, `value`) VALUES ('1', 'RON', 'Ron 0.5', '0.5');
INSERT INTO `vendingmachinedatabase`.`coin` (`id`, `type`, `description`, `value`) VALUES ('2', 'RON', 'Ron 0.1', '0.1');
INSERT INTO `vendingmachinedatabase`.`coin` (`id`, `type`, `description`, `value`) VALUES ('3', 'RON', 'Ron 1.0', '1.0');

CREATE TABLE `vendingmachinedatabase`.`coin_stock` (
  `id` INT NOT NULL,
  `id_coin` INT NULL,
  `number_of_coins` INT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `id_coin_idx` (`id_coin` ASC),
  CONSTRAINT `id_coin`
    FOREIGN KEY (`id_coin`)
    REFERENCES `vendingmachinedatabase`.`coin` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
    
INSERT INTO `vendingmachinedatabase`.`coin_stock` (`id`, `id_coin`, `number_of_coins`) VALUES ('1', '1', '50');
INSERT INTO `vendingmachinedatabase`.`coin_stock` (`id`, `id_coin`, `number_of_coins`) VALUES ('2', '2', '40');
INSERT INTO `vendingmachinedatabase`.`coin_stock` (`id`, `id_coin`, `number_of_coins`) VALUES ('3', '3', '50');

CREATE TABLE `vendingmachinedatabase`.`ticket` (
  `id` INT NOT NULL,
  `type` VARCHAR(45) NULL,
  `description` VARCHAR(45) NULL,
  `value` DOUBLE NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `idticket_UNIQUE` (`id` ASC));

INSERT INTO `vendingmachinedatabase`.`ticket` (`id`, `type`, `description`, `value`) VALUES ('1', 'RATP', 'RATP 1way', '1.5');
INSERT INTO `vendingmachinedatabase`.`ticket` (`id`, `type`, `description`, `value`) VALUES ('2', 'RATP', 'RATP 2ways', '2.5');
INSERT INTO `vendingmachinedatabase`.`ticket` (`id`, `type`, `description`, `value`) VALUES ('3', 'TRAIN', 'TRAIN 1way', '2');
INSERT INTO `vendingmachinedatabase`.`ticket` (`id`, `type`, `description`, `value`) VALUES ('4', 'BLUEAIR', 'Moscova-Bucuresti', '1');


CREATE TABLE `vendingmachinedatabase`.`ticket_stock` (
  `id` INT NOT NULL,
  `id_ticket` INT NULL,
  `number_of_tickets` INT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  UNIQUE INDEX `id_ticket_UNIQUE` (`id_ticket` ASC));
  
INSERT INTO `vendingmachinedatabase`.`ticket_stock` (`id`, `id_ticket`, `number_of_tickets`) VALUES ('1', '1', '50');
INSERT INTO `vendingmachinedatabase`.`ticket_stock` (`id`, `id_ticket`, `number_of_tickets`) VALUES ('2', '2', '30');
INSERT INTO `vendingmachinedatabase`.`ticket_stock` (`id`, `id_ticket`, `number_of_tickets`) VALUES ('3', '3', '5');
INSERT INTO `vendingmachinedatabase`.`ticket_stock` (`id`, `id_ticket`, `number_of_tickets`) VALUES ('4', '4', '4');


