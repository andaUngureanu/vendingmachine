package pentastagiu.vending_machine.initializer;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import pentastagiu.vending_machine.data_base.DataBase;
import pentastagiu.vending_machine.model.Coin;
import pentastagiu.vending_machine.model.Credit;
import pentastagiu.vending_machine.model.StockCoins;
import pentastagiu.vending_machine.model.StockTickets;
import pentastagiu.vending_machine.model.Ticket;
import pentastagiu.vending_machine.model.TicketsAndChange;
import pentastagiu.vending_machine.model.VendingMachine;
import pentastagiu.vending_machine.service.VendingMachineService;
import pentastagiu.vending_machine.service.impl.VendingMachineServiceImpl;
import pentastagiu.vending_machine.user_interface.VendingMachineUIService;
import pentastagiu.vending_machine.user_interface.impl.VendingMachineUIServiceImpl;

/**
 * Initializer of a vending machine.
 * 
 * @author Anda Ungureanu
 *
 */

public class VendingMachineInitializer {
	/***
	 * Create a StockTickets with the informations in the file.
	 * 
	 * @param fileToParse
	 * @return
	 */
	public StockTickets getTicketStockFromDataBase(DataBase dataBase) {
		Map<Ticket, Integer> ticketsList = new HashMap<Ticket, Integer>();
		StockTickets stockTicket;
		try {
			ResultSet resultSet = dataBase.getTicketStocks();
			while (resultSet.next()) {
				Ticket ticket = new Ticket();
				ticket.setType(resultSet.getString("type"));
				ticket.setDescription(resultSet.getString("description"));
				ticket.setValue(resultSet.getBigDecimal("value"));
				ticketsList.put(ticket, resultSet.getInt("number_of_tickets"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		stockTicket = new StockTickets(ticketsList);
		return stockTicket;
	}

	/**
	 * Create a StockCoins with the informations in the file.
	 * 
	 * @param fileToParse
	 * @return
	 */
	public StockCoins getCoinStockFromDataBase(DataBase dataBase) {
		Map<Coin, Integer> coinList = new HashMap<Coin, Integer>();
		StockCoins stockCoin;
		try {
			ResultSet resultSet = dataBase.getCoinStocks();
			while (resultSet.next()) {
				Coin coin = new Coin();
				coin.setType(resultSet.getString("type"));
				coin.setDescription(resultSet.getString("description"));
				coin.setValue(resultSet.getBigDecimal("value"));
				coinList.put(coin, resultSet.getInt("number_of_coins"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		stockCoin = new StockCoins(coinList);
		return stockCoin;
	}

	/**
	 * Instantiates a vending machine, and a vending machine UI in order to
	 * return a vendingMachineService.
	 * 
	 * @param ticketFile
	 * @param coinFile
	 * @return
	 */
	public VendingMachineService initialize() {
		DataBase dataBase = new DataBase();
		try {
			dataBase.connectToDataBase("vendingmachinedatabase");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Credit machineCredit = new Credit(getCoinStockFromDataBase(dataBase), new BigDecimal(0));
		machineCredit.updateTotalCreditValue();

		StockCoins stockCoin = initializeCreditStock(machineCredit);
		Credit inputCredit = new Credit(stockCoin, new BigDecimal(0));

		StockCoins stockCoinForTicketsAndChange = initializeCreditStock(machineCredit);
		Credit creditInTicketsAndChange = new Credit(stockCoinForTicketsAndChange, new BigDecimal(0));

		StockTickets machineTickets = getTicketStockFromDataBase(dataBase);
		Map<Ticket, Integer> ticketList = new HashMap<Ticket, Integer>();
		for (Ticket ticket : machineTickets.getStock().keySet()) {
			ticketList.put(ticket, 0);
		}
		StockTickets ticketsInTicketsAndChange = new StockTickets(ticketList);
		TicketsAndChange ticketsAndChange = new TicketsAndChange(ticketsInTicketsAndChange, creditInTicketsAndChange);

		VendingMachine vendingMachine = new VendingMachine(machineTickets, machineCredit, inputCredit,
				ticketsAndChange);
		VendingMachineUIService machineUI = new VendingMachineUIServiceImpl();
		VendingMachineService vendingMachineService = new VendingMachineServiceImpl(vendingMachine, machineUI);
		return vendingMachineService;
	}

	/**
	 * Creates a StockCoins using the map returned by initializeInputCredit
	 * function.
	 * 
	 * @param machineCredit
	 * @return
	 */
	public StockCoins initializeCreditStock(Credit machineCredit) {
		Map<Coin, Integer> coinList = initializeInputCredit(machineCredit);
		StockCoins stockCoin = new StockCoins(coinList);
		return stockCoin;
	}

	/**
	 * Puts in a Map with all types of coins as keys and 0 as value.
	 * 
	 * @param machineCredit
	 * @return
	 */
	public Map<Coin, Integer> initializeInputCredit(Credit machineCredit) {
		Map<Coin, Integer> coinList = new HashMap<Coin, Integer>();
		for (Coin coin : machineCredit.getCreditStock().getStock().keySet()) {
			coinList.put(coin, 0);
		}
		return coinList;
	}
}
