package pentastagiu.vending_machine.user_interface.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

import pentastagiu.vending_machine.model.Coin;
import pentastagiu.vending_machine.model.Credit;
import pentastagiu.vending_machine.model.StockTickets;
import pentastagiu.vending_machine.model.Ticket;
import pentastagiu.vending_machine.model.TicketsAndChange;
import pentastagiu.vending_machine.user_interface.VendingMachineUIService;

/**
 * Implements the display of a vending machine.
 * 
 * @author Anda Ungureanu
 *
 */
public class VendingMachineUIServiceImpl implements VendingMachineUIService {

	private Scanner userInput = new Scanner(System.in);

	public List<Ticket> initialMenuPrint(StockTickets stock) {
		Set<String> types = new TreeSet<String>();
		int i = 1;
		System.out.println("Choose ticket type :");
		for (Ticket ticket : stock.getStock().keySet()) {
			types.add(ticket.getType());

		}
		for (String ticketType : types) {
			System.out.println(i + " - " + ticketType);
			i++;
		}
		List<Ticket> ticketsList = new ArrayList<Ticket>();
		String[] typesList = new String[types.size()];
		types.toArray(typesList);
		System.out.print("Optiune : ");
		int op = readInt();
		System.out.println("Choose trip :");
		i = 1;
		for (Ticket ticket : stock.getStock().keySet()) {
			if (ticket.getType().equals(typesList[op - 1])) {
				ticketsList.add(ticket);
				System.out.println(i + "-" + ticket.getDescription());
				i++;
			}
		}
		System.out.print("Optiune : ");
		return ticketsList;

	}

	public void getNumberOfTicketsPrint() {
		System.out.print("Number of tickets : ");
	}

	public int readInt() {
		userInput = null;
		userInput = new Scanner(System.in);
		int userInputOption = 0;
		if (userInput.hasNextInt()) {
			userInputOption = userInput.nextInt();
		} else {
			invalidInputPrint();
			userInput = null;
			userInput = new Scanner(System.in);
			System.out.println();
		}
		return userInputOption;

	}

	public BigDecimal readBigDecimal() {
		userInput = null;
		userInput = new Scanner(System.in);
		BigDecimal userInputOption = new BigDecimal(0);
		if (userInput.hasNextBigDecimal()) {
			userInputOption = userInput.nextBigDecimal();
		} else {
			invalidInputPrint();
			userInput = null;
			userInput = new Scanner(System.in);
		}

		return userInputOption;
	}

	public void unableToBuyTicketPrint() {
		System.out.println("~Ticket not available~");
	}

	public void ticketInformationsPrint(Ticket ticket) {
		System.out.println("Ticket type selected : " + ticket.getDescription());
		System.out.println("Price : " + ticket.getValue());

	}

	public void waitPaymentPrint() {
		System.out.print("Coin : ");
	}

	public void invalidInputPrint() {
		System.out.println("Invalid input!");

	}

	public void giveTicketPrint() {
		System.out.println("~Please take your ticket~");

	}

	public void orderCostPrint(BigDecimal cost) {
		System.out.println("Total Cost : " + cost);
	}

	public void ticketsAndChangePrint(TicketsAndChange output) {
		for (Map.Entry<Ticket, Integer> entry : output.getTicketsInTicketsAndChange().getStock().entrySet()) {
			if (entry.getValue() > 0)
				System.out.println(entry.getKey().getDescription() + " : " + entry.getValue());
		}
		creditPrint(output.getCreditInTicketsAndChange());
	}

	public void creditPrint(Credit credit) {
		for (Map.Entry<Coin, Integer> entry : credit.getCreditStock().getStock().entrySet()) {
			if (entry.getValue() > 0)
				System.out.println(entry.getKey().getDescription() + " : " + entry.getValue());
		}
	}

}
