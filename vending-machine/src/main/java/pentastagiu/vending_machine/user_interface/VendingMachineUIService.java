package pentastagiu.vending_machine.user_interface;

import java.math.BigDecimal;
import java.util.List;

import pentastagiu.vending_machine.model.Credit;
import pentastagiu.vending_machine.model.StockTickets;
import pentastagiu.vending_machine.model.Ticket;
import pentastagiu.vending_machine.model.TicketsAndChange;

/**
 * Interface for the display of a vending machine.
 * 
 * @author Anda Ungureanu
 *
 */
public interface VendingMachineUIService {
	/**
	 * Prints initial menu .
	 * 
	 * @return
	 */
	public List<Ticket> initialMenuPrint(StockTickets stock);

	/**
	 * Reads an int as input.
	 */
	public int readInt();

	/**
	 * Asks user for the number of tickets.
	 */
	public void getNumberOfTicketsPrint();

	/**
	 * Reads a double as input.
	 */
	public BigDecimal readBigDecimal();

	/**
	 * Informs the user that tickets can't be bought.
	 */
	public void unableToBuyTicketPrint();

	/**
	 * Prints the type and the cost of a ticket.
	 * 
	 * @param ticket
	 */
	public void ticketInformationsPrint(Ticket ticket);

	/**
	 * Informs the user to pay.
	 */
	public void waitPaymentPrint();

	/**
	 * Prints an invalid input message.
	 */
	public void invalidInputPrint();

	/**
	 * Prints a message to inform user to take his ticket.
	 */
	public void giveTicketPrint();

	/**
	 * Prints the change in TicketsAndChange
	 */
	public void ticketsAndChangePrint(TicketsAndChange user);

	/**
	 * Prints the final cost.
	 */
	public void orderCostPrint(BigDecimal cost);

	/**
	 * Prints the number of coins of each type stored in Credit.
	 * 
	 * @param credit
	 */
	public void creditPrint(Credit credit);
}
