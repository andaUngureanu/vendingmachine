package pentastagiu.vending_machine.service;

import java.math.BigDecimal;

import pentastagiu.vending_machine.model.Coin;
import pentastagiu.vending_machine.model.Credit;
import pentastagiu.vending_machine.model.StockTickets;
import pentastagiu.vending_machine.model.Ticket;
import pentastagiu.vending_machine.model.TicketsAndChange;
import pentastagiu.vending_machine.model.VendingMachine;

/**
 * Interface for functions of a vending machine.
 * 
 * @author Anda Ungureanu
 *
 */
public interface VendingMachineService {

	/**
	 * Provides user interface and reads input.
	 */
	public void interact();

	/**
	 * Divides the given change in coins and uploads the Credit in
	 * TicketAndChange.
	 */
	public void prepareCreditInTicketAndChange(BigDecimal change);

	/**
	 * Returns a vending Machine.
	 */
	public VendingMachine getVendingMachine();

	/**
	 * Reads the value of each coin introduced by the user in the machine.
	 */
	public boolean takePayment(BigDecimal cost);

	/**
	 * Sets the credit for 0 coins.
	 */
	public void resetCredit(Credit credit);

	/**
	 * Adds to credit_dest the credit in credit_src.
	 */
	public void addCredit(Credit credit_dest, Credit credit_src);

	/**
	 * Substracts credit_src form crdit_dest.
	 */
	public void substractCredit(Credit credit_dest, Credit credit_src);

	/**
	 * Sets the ticketAndChange variables on 0.
	 */
	public void resetTicketsAndChange(TicketsAndChange ticketsAndChange);

	/**
	 * Returns the cost of a specific number of tickets.
	 */
	public BigDecimal orderCost(Ticket ticket, int numberOfTickets);

	/**
	 * Calculates the change for the input cost.
	 */
	public BigDecimal getChange(BigDecimal cost);

	/**
	 * Checks if the value is valid for the coin types. If it is returns a coin
	 * with the given value else returns null.
	 * 
	 * @param value
	 * @return
	 */
	public Coin getCoinByValue(BigDecimal value);

	/**
	 * Increases the number of the given coin in credit with one.
	 * 
	 * @param coin
	 * @param credit
	 */
	public void addCoinToCredit(Coin coin, Credit credit);

	/**
	 * Substracts the tickets in second stock of tickets given as parameter from
	 * the first stock of tickets.
	 * 
	 * @param stock_dest
	 * @param stock_src
	 */
	public void substractTickets(StockTickets stock_dest, StockTickets stock_src);
}
