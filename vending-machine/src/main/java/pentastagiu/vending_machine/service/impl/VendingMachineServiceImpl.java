package pentastagiu.vending_machine.service.impl;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import pentastagiu.vending_machine.data_base.DataBase;
import pentastagiu.vending_machine.model.Coin;
import pentastagiu.vending_machine.model.Credit;
import pentastagiu.vending_machine.model.StockTickets;
import pentastagiu.vending_machine.model.Ticket;
import pentastagiu.vending_machine.model.TicketsAndChange;
import pentastagiu.vending_machine.model.VendingMachine;
import pentastagiu.vending_machine.service.VendingMachineService;
import pentastagiu.vending_machine.user_interface.VendingMachineUIService;

/**
 * Implements a vending machine and it's display.
 * 
 * @author Anda Ungureanu
 *
 */
public class VendingMachineServiceImpl implements VendingMachineService {
	private VendingMachine vendingMachine;
	private VendingMachineUIService machineUI;

	public VendingMachineServiceImpl(VendingMachine vendingMachine, VendingMachineUIService machineUI) {
		super();
		this.vendingMachine = vendingMachine;
		this.machineUI = machineUI;
	}

	public void substractTickets(StockTickets stock_dest, StockTickets stock_src) {
		for (Map.Entry<Ticket, Integer> entry : stock_src.getStock().entrySet()) {
			stock_dest.getStock().put(entry.getKey(), stock_dest.getStock().get(entry.getKey()) - entry.getValue());
		}
	}

	public void substractCredit(Credit credit_dest, Credit credit_src) {
		for (Map.Entry<Coin, Integer> entry : credit_src.getCreditStock().getStock().entrySet()) {

			vendingMachine.getMachineCredit().getCreditStock().getStock().put(entry.getKey(),
					credit_dest.getCreditStock().getStock().get(entry.getKey()) - entry.getValue());
		}
		credit_dest.updateTotalCreditValue();
	}

	public void addCredit(Credit credit_dest, Credit credit_src) {
		for (Map.Entry<Coin, Integer> entry : credit_src.getCreditStock().getStock().entrySet()) {
			credit_dest.getCreditStock().getStock().put(entry.getKey(),
					entry.getValue() + credit_dest.getCreditStock().getStock().get(entry.getKey()));
		}

		credit_dest.updateTotalCreditValue();
	}

	public void resetTicketsAndChange(TicketsAndChange ticketsAndChange) {
		resetCredit(ticketsAndChange.getCreditInTicketsAndChange());
		for (Ticket ticket : vendingMachine.getMachineTickets().getStock().keySet()) {
			ticketsAndChange.getTicketsInTicketsAndChange().getStock().put(ticket, 0);
		}
	}

	public void resetCredit(Credit credit) {
		for (Coin coin : vendingMachine.getInputCredit().getCreditStock().getStock().keySet()) {
			credit.getCreditStock().getStock().put(coin, 0);
		}
		credit.setTotalCreditValue(new BigDecimal(0));
	}

	public BigDecimal orderCost(Ticket ticket, int numberOfTickets) {
		BigDecimal cost = ticket.getValue().multiply(new BigDecimal(numberOfTickets));
		return cost;
	}

	public Coin getCoinByValue(BigDecimal value) {
		for (Coin coin : vendingMachine.getMachineCredit().getCreditStock().getStock().keySet()) {
			if (value.compareTo(coin.getValue()) == 0) {
				return coin;
			}
		}
		return null;
	}

	public void addCoinToCredit(Coin coin, Credit credit) {
		int numberOfCoinsBefore = credit.getCreditStock().getStock().get(coin);
		credit.getCreditStock().getStock().put(coin, numberOfCoinsBefore + 1);
		credit.updateTotalCreditValue();
	}

	public boolean takePayment(BigDecimal cost) {
		Credit inputCredit = vendingMachine.getInputCredit();
		while (inputCredit.getTotalCreditValue().compareTo(cost) == -1) {
			machineUI.waitPaymentPrint();
			BigDecimal coinValue = machineUI.readBigDecimal();
			Coin coin = getCoinByValue(coinValue);
			if (coin != null) {
				addCoinToCredit(coin, inputCredit);
				inputCredit.updateTotalCreditValue();
			} else
				machineUI.invalidInputPrint();
		}
		return true;
	}

	public BigDecimal getChange(BigDecimal cost) {

		BigDecimal outputChange = vendingMachine.getInputCredit().getTotalCreditValue().subtract(cost);

		return outputChange;
	}

	public void prepareCreditInTicketAndChange(BigDecimal change) {
		Map<Coin, Integer> coinList = new TreeMap<Coin, Integer>(
				vendingMachine.getMachineCredit().getCreditStock().getStock());
		for (Coin coin : coinList.keySet()) {
			int numberOfCoins = change.divide(coin.getValue()).intValue();
			change = change.subtract(coin.getValue().multiply(new BigDecimal(numberOfCoins)));
			vendingMachine.getTicketsAndChange().getCreditInTicketsAndChange().getCreditStock().getStock().put(coin,
					numberOfCoins);
		}
		vendingMachine.getTicketsAndChange().getCreditInTicketsAndChange().updateTotalCreditValue();
	}

	public void interact() {
		while (true) {

			List<Ticket> ticketList = machineUI.initialMenuPrint(vendingMachine.getMachineTickets());
			int userInputOption = machineUI.readInt();
			if (userInputOption > 0 && userInputOption <= vendingMachine.getMachineTickets().getStock().size()) {
				Ticket ticket = ticketList.get(userInputOption - 1);
				if (ticket != null) {
					machineUI.ticketInformationsPrint(ticket);
					machineUI.getNumberOfTicketsPrint();
					int numberOfTickets = machineUI.readInt();
					int availableNumberOfTickets = vendingMachine.getMachineTickets().getStock().get(ticket);
					if (numberOfTickets > 0 && numberOfTickets <= availableNumberOfTickets) {
						vendingMachine.getTicketsAndChange().getTicketsInTicketsAndChange().getStock().put(ticket,
								numberOfTickets);
						BigDecimal cost = orderCost(ticket, numberOfTickets);
						machineUI.orderCostPrint(cost);
						boolean validPayment = takePayment(cost);
						if (validPayment) {
							try {
								if (availableNumberOfTickets == numberOfTickets) {
									DataBase.removeTicketStockInDataBase(ticket);
									DataBase.removeTicketInDataBase(ticket);
								} else
									DataBase.updateTicketStocksInDataBase(numberOfTickets, ticket);
							} catch (SQLException e) {
								e.printStackTrace();
							}
							BigDecimal outputChange = getChange(cost);
							prepareCreditInTicketAndChange(outputChange);
							machineUI.ticketsAndChangePrint(vendingMachine.getTicketsAndChange());
							substractCredit(vendingMachine.getMachineCredit(),
									vendingMachine.getTicketsAndChange().getCreditInTicketsAndChange());
							addCredit(vendingMachine.getMachineCredit(), vendingMachine.getInputCredit());
							resetCredit(vendingMachine.getInputCredit());
							resetTicketsAndChange(vendingMachine.getTicketsAndChange());
							try {
								DataBase.updateCoinStocksInDataBase(vendingMachine.getMachineCredit());
							} catch (SQLException e) {
								e.printStackTrace();
							}
						} else
							machineUI.unableToBuyTicketPrint();
					}
				}
			}
		}
	}

	public VendingMachine getVendingMachine() {
		return vendingMachine;
	}

}
