package pentastagiu.vending_machine.model;

import java.math.BigDecimal;

/**
 * Represents a coin accepted by the vending machine.
 * 
 * @author Anda Ungureanu
 *
 */
public class Coin implements Comparable<Coin> {
	private String type;
	private String description;
	private BigDecimal value;

	public Coin() {
	}

	public Coin(String type, String description, BigDecimal value) {
		super();
		this.type = type;
		this.description = description;
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public int compareTo(Coin coin) {
		return coin.getValue().compareTo(this.value);
	}

}
