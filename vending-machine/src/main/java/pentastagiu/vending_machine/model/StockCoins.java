package pentastagiu.vending_machine.model;

import java.util.Map;

/**
 * Represents a stock of coins.
 * 
 * @author Anda Ungureanu
 */
public class StockCoins {
	private Map<Coin, Integer> stock;

	public StockCoins() {

	}

	public StockCoins(Map<Coin, Integer> stock) {
		super();
		this.stock = stock;
	}

	public Map<Coin, Integer> getStock() {
		return stock;
	}

	public void setStock(Map<Coin, Integer> stock) {
		this.stock = stock;
	}
}
