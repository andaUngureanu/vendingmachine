package pentastagiu.vending_machine.model;

/**
 * Represents a vending machine . The machine can interact with the user .
 * 
 * @author Anda Ungureanu
 */
public class VendingMachine {
	private StockTickets machineTickets;
	private Credit machineCredit;
	private Credit inputCredit;
	private TicketsAndChange ticketsAndChange;

	public VendingMachine() {

	}

	public VendingMachine(StockTickets machineTickets, Credit machineCredit, Credit inputCredit,
			TicketsAndChange ticketsAndChange) {
		super();
		this.machineTickets = machineTickets;
		this.machineCredit = machineCredit;
		this.inputCredit = inputCredit;
		this.ticketsAndChange = ticketsAndChange;
	}

	public StockTickets getMachineTickets() {
		return machineTickets;
	}

	public void setMachineTickets(StockTickets machineTickets) {
		this.machineTickets = machineTickets;
	}

	public Credit getMachineCredit() {
		return machineCredit;
	}

	public void setMachineCredit(Credit machineCredit) {
		this.machineCredit = machineCredit;
	}

	public Credit getInputCredit() {
		return inputCredit;
	}

	public void setInputCredit(Credit inputCredit) {
		this.inputCredit = inputCredit;
	}

	public TicketsAndChange getTicketsAndChange() {
		return ticketsAndChange;
	}

	public void setTicketsAndChange(TicketsAndChange ticketsAndChange) {
		this.ticketsAndChange = ticketsAndChange;
	}

}
