package pentastagiu.vending_machine.model;

import java.util.Map;

/**
 * Represents a stock of tickets.
 * 
 * @author Anda Ungureanu
 */
public class StockTickets {

	Map<Ticket, Integer> stock;

	public StockTickets() {
		super();
	}

	public StockTickets(Map<Ticket, Integer> stock) {
		super();
		this.stock = stock;
	}

	public Map<Ticket, Integer> getStock() {
		return stock;
	}

	public void setStock(Map<Ticket, Integer> stock) {
		this.stock = stock;
	}

}
