package pentastagiu.vending_machine.model;

import java.math.BigDecimal;

/**
 * Represents a ticket printed by the vending machine.
 * 
 * @author Anda Ungureanu
 * 
 */
public class Ticket {
	private BigDecimal value;
	private String type;
	private String description;

	public Ticket() {
	}

	public Ticket(String type, String description, BigDecimal value) {
		super();
		this.value = value;
		this.type = type;
		this.description = description;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
