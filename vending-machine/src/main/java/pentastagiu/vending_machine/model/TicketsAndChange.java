package pentastagiu.vending_machine.model;

/**
 * Used to stock tickets and credit.
 * 
 * @author Anda Ungureanu
 *
 */
public class TicketsAndChange {
	private StockTickets ticketsInTicketsAndChange;
	private Credit creditInTicketsAndChange;

	public TicketsAndChange() {
	}

	public TicketsAndChange(StockTickets ticketsInTicketsAndChange, Credit creditInTicketsAndChange) {
		super();
		this.ticketsInTicketsAndChange = ticketsInTicketsAndChange;
		this.creditInTicketsAndChange = creditInTicketsAndChange;
	}

	public StockTickets getTicketsInTicketsAndChange() {
		return ticketsInTicketsAndChange;
	}

	public void setTicketsInTicketsAndChange(StockTickets ticketsInTicketsAndChange) {
		this.ticketsInTicketsAndChange = ticketsInTicketsAndChange;
	}

	public Credit getCreditInTicketsAndChange() {
		return creditInTicketsAndChange;
	}

	public void setCreditInTicketsAndChange(Credit creditInTicketsAndChange) {
		this.creditInTicketsAndChange = creditInTicketsAndChange;
	}

}
