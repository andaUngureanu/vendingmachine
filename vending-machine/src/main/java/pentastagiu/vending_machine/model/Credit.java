package pentastagiu.vending_machine.model;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Class that keeps track of money.
 * 
 * @author Anda Ungureanu
 */
public class Credit {
	private StockCoins creditStock;
	private BigDecimal totalCreditValue;

	/**
	 * Makes sure that the value in TotalCreditValue is equal with the total
	 * value of all coins in Credit.
	 */

	public Credit() {
		super();
	}

	public Credit(StockCoins creditStock, BigDecimal totalCreditValue) {
		super();
		this.creditStock = creditStock;
		this.totalCreditValue = totalCreditValue;
	}

	public StockCoins getCreditStock() {
		return creditStock;
	}

	public void setCreditStock(StockCoins creditStock) {
		this.creditStock = creditStock;
	}

	public BigDecimal getTotalCreditValue() {
		return totalCreditValue;
	}

	public void setTotalCreditValue(BigDecimal totalCreditValue) {
		this.totalCreditValue = totalCreditValue;
	}

	/**
	 * Sets the value of totalCreditValue as the total value sum of the coins in
	 * the creditStock.
	 */
	public void updateTotalCreditValue() {
		totalCreditValue = new BigDecimal(0);
		for (Map.Entry<Coin, Integer> entry : creditStock.getStock().entrySet()) {
			totalCreditValue = totalCreditValue
					.add(entry.getKey().getValue().multiply(new BigDecimal(entry.getValue())));
		}

	}

}
