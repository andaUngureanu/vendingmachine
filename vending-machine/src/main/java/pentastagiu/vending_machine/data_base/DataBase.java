package pentastagiu.vending_machine.data_base;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import pentastagiu.vending_machine.model.Coin;
import pentastagiu.vending_machine.model.Credit;
import pentastagiu.vending_machine.model.Ticket;

public class DataBase {
	private static Connection connection;

	public void connectToDataBase(String dataBaseName) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");

		connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + dataBaseName, "root", "0000");
	}

	public ResultSet getTicketStocks() throws SQLException {
		String query = "SELECT ticket.type,ticket.description,ticket.value,ticket_stock.number_of_tickets from ticket inner join ticket_stock on ticket.id=ticket_stock.id_ticket;";
		PreparedStatement statement = connection.prepareStatement(query);
		return statement.executeQuery(query);
	}

	public static int updateTicketStocksInDataBase(int numberOfTicketsToBuy, Ticket ticket) throws SQLException {
		String query = "UPDATE ticket_stock SET number_of_tickets = number_of_tickets -" + numberOfTicketsToBuy
				+ " where id_ticket=(SELECT id from ticket where description = '" + ticket.getDescription() + "' );";
		PreparedStatement statement = connection.prepareStatement(query);
		return statement.executeUpdate(query);
	}

	public static int removeTicketStockInDataBase(Ticket ticket) throws SQLException {
		String query = "DELETE from ticket_stock where id_ticket =(SELECT id from ticket where description = '"
				+ ticket.getDescription() + "' );";
		PreparedStatement statement = connection.prepareStatement(query);
		return statement.executeUpdate();
	}

	public static int removeTicketInDataBase(Ticket ticket) throws SQLException {
		String query = "DELETE from ticket where description = '" + ticket.getDescription() + "' ;";
		PreparedStatement statement = connection.prepareStatement(query);
		return statement.executeUpdate();
	}

	public ResultSet getCoinStocks() throws SQLException {
		String query = "SELECT coin.type,coin.description,coin.value,coin_stock.number_of_coins from coin inner join coin_stock on coin.id=coin_stock.id_coin;";
		PreparedStatement statement = connection.prepareStatement(query);

		return statement.executeQuery(query);
	}

	public static void updateCoinStocksInDataBase(Credit credit) throws SQLException {
		for (Coin coin : credit.getCreditStock().getStock().keySet()) {
			String query = "UPDATE coin_stock SET number_of_coins = " + credit.getCreditStock().getStock().get(coin)
					+ " where id_coin =(SELECT id from coin where description = '" + coin.getDescription() + "' );";
			PreparedStatement statement;
			try {
				statement = connection.prepareStatement(query);
				statement.executeUpdate();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		DataBase.connection = connection;
	}

}
