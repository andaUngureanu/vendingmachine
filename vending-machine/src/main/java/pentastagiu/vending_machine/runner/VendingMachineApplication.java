package pentastagiu.vending_machine.runner;

import pentastagiu.vending_machine.initializer.VendingMachineInitializer;
import pentastagiu.vending_machine.service.VendingMachineService;

/**
 * Starts the services for a vending machine.
 */
public class VendingMachineApplication {

	public static void main(String[] args) {
		VendingMachineInitializer vendingMachineInitializer = new VendingMachineInitializer();
		VendingMachineService vendingMachineService = vendingMachineInitializer.initialize();
		vendingMachineService.interact();

	}
}
