package pentastagiu.vending_machine.test;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import pentastagiu.vending_machine.initializer.VendingMachineInitializer;
import pentastagiu.vending_machine.model.Coin;
import pentastagiu.vending_machine.model.Credit;
import pentastagiu.vending_machine.model.StockTickets;
import pentastagiu.vending_machine.model.Ticket;
import pentastagiu.vending_machine.model.TicketsAndChange;
import pentastagiu.vending_machine.service.VendingMachineService;

public class VendingMachineServiceImplTest {
	VendingMachineService vendingMachineService;

	@Before
	public void initVendingMachine() {
		VendingMachineInitializer vendingMachineInitializer = new VendingMachineInitializer();
		vendingMachineService = vendingMachineInitializer.initialize();
	}

	@After
	public void cleanVendingMachine() {
		vendingMachineService = null;
	}

	@Test
	public void testOrderCost() {
		Ticket ticket = new Ticket("Type", "Description", new BigDecimal(3));
		BigDecimal costTest = vendingMachineService.orderCost(ticket, 2);
		BigDecimal costVerify = new BigDecimal(6);
		assertEquals(costTest, costVerify);
	}

	@Test
	public void testGetChange() {
		vendingMachineService.getVendingMachine().getInputCredit().setTotalCreditValue(new BigDecimal(23));
		BigDecimal changeTest = vendingMachineService.getChange(new BigDecimal(10));
		BigDecimal changeVerify = new BigDecimal(13);
		assertEquals(changeTest, changeVerify);
	}

	@Test
	public void getCoinByValueReturnsCoin() {
		Coin coinTest = vendingMachineService.getCoinByValue(new BigDecimal(0.5));
		BigDecimal coinValueVerify = new BigDecimal(0.5);
		assertEquals(coinTest.getValue(), coinValueVerify);
	}

	@Test
	public void getCoinByValueReturnsNull() {
		Coin coinTest = vendingMachineService.getCoinByValue(new BigDecimal(0));
		assertEquals(coinTest, null);
	}

	@Test
	public void testSubstractTickets() {
		StockTickets vendingMachineStock = vendingMachineService.getVendingMachine().getMachineTickets();
		Integer numberOfTicketsVerify = vendingMachineStock.getStock().entrySet().iterator().next().getValue();
		StockTickets ticketsToAdd = vendingMachineService.getVendingMachine().getTicketsAndChange()
				.getTicketsInTicketsAndChange();
		ticketsToAdd.getStock().put(ticketsToAdd.getStock().entrySet().iterator().next().getKey(), 2);
		vendingMachineService.substractTickets(vendingMachineStock, ticketsToAdd);
		numberOfTicketsVerify = numberOfTicketsVerify - 2;
		assertEquals(vendingMachineStock.getStock().entrySet().iterator().next().getValue(), numberOfTicketsVerify);
	}

	@Test
	public void testAddCredit() {
		Credit vendingMachineCredit = vendingMachineService.getVendingMachine().getMachineCredit();
		Integer numberOfCoinsVerify = vendingMachineCredit.getCreditStock().getStock().entrySet().iterator().next()
				.getValue();
		Credit coinsToAdd = vendingMachineService.getVendingMachine().getTicketsAndChange()
				.getCreditInTicketsAndChange();
		coinsToAdd.getCreditStock().getStock()
				.put(coinsToAdd.getCreditStock().getStock().entrySet().iterator().next().getKey(), 2);
		vendingMachineService.addCredit(vendingMachineCredit, coinsToAdd);
		numberOfCoinsVerify = numberOfCoinsVerify + 2;
		assertEquals(vendingMachineCredit.getCreditStock().getStock().entrySet().iterator().next().getValue(),
				numberOfCoinsVerify);
	}

	@Test
	public void testResetTicketsAndChange() {
		TicketsAndChange ticketsAndChange = vendingMachineService.getVendingMachine().getTicketsAndChange();
		ticketsAndChange.setCreditInTicketsAndChange(vendingMachineService.getVendingMachine().getMachineCredit());
		vendingMachineService.resetTicketsAndChange(ticketsAndChange);
		int numberOfTicketsVerify = ticketsAndChange.getTicketsInTicketsAndChange().getStock().entrySet().iterator()
				.next().getValue();
		assertEquals(numberOfTicketsVerify, 0);
	}

	@Test
	public void testResetCredit() {
		Credit vendingMachineCredit = vendingMachineService.getVendingMachine().getMachineCredit();
		vendingMachineService.resetCredit(vendingMachineCredit);
		int numberOfCoinsVerify = vendingMachineCredit.getCreditStock().getStock().entrySet().iterator().next()
				.getValue();
		assertEquals(numberOfCoinsVerify, 0);
	}

	@Test
	public void testAddCoinToCredit() {
		Credit vendingMachineCredit = vendingMachineService.getVendingMachine().getMachineCredit();
		int initialNumberOfCoins = vendingMachineCredit.getCreditStock().getStock().entrySet().iterator().next()
				.getValue();
		Coin coinToAdd = vendingMachineCredit.getCreditStock().getStock().keySet().iterator().next();
		vendingMachineService.addCoinToCredit(coinToAdd, vendingMachineCredit);
		int numberOfCoinsVerify = vendingMachineCredit.getCreditStock().getStock().entrySet().iterator().next()
				.getValue();
		assertEquals(numberOfCoinsVerify, initialNumberOfCoins + 1);
	}

	@Test
	public void testPrepareCreditInTicketAndChange() {
		Credit ticketsAndChangeCredit = vendingMachineService.getVendingMachine().getTicketsAndChange()
				.getCreditInTicketsAndChange();
		vendingMachineService.prepareCreditInTicketAndChange(new BigDecimal(0.5));
		assertEquals(ticketsAndChangeCredit.getTotalCreditValue(), new BigDecimal(0.5));
	}
}