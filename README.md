# README #

Vending Machine

### What is this repository for? ###

* Vending Machine is an application meant to simulate an electric machine capabile to let the user select a type of ticket from a list, introduce the number of tickets and offer the ticket after a certain sum of money has been put in the machine.


### Configuration details ###

* If you want to use Vending Machine, you can also create the required database tables by running the script "vendingmachinedata.sql" from /database .